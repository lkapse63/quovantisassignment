package org.lucky.session.assignment

import org.apache.log4j.Logger
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object App {

  @transient lazy val log : Logger = Logger.getLogger(getClass)
  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("student-marks")
      .master("local[*]")
      .getOrCreate()

    val marksDF = spark
      .read
      .format("csv")
      .option("inferSchema","true")
      .option("header","true")
      .option("path","marks.csv")
      .load()

    val studentDF = spark
      .read
      .format("csv")
      .option("inferSchema","true")
      .option("header","true")
      .option("path","student.csv")
      .load()

//     1. >90 A, 90-80 B, 80-70 C, 40-70 D and <40 F
 //    4. Average percentage of students.
    val gradeDF = marksDF
      .withColumn("total_marks", expr("english + hindi + math + science") )
      .withColumn("grade",
         when(col("total_marks").>("90"),"A")
        .when(col("total_marks").between(81,90),"B")
        .when(col("total_marks").between(71,80),"C")
        .when(col("total_marks").between(40,70),"D")
        .otherwise("F"))
      .withColumn("avg_marks", expr(" total_marks / 4"))

     writeDf(gradeDF,"assignment/grade-avg")
     log.info("grade output done")


    val joinDF=studentDF.join(gradeDF,studentDF.col("student_id") === gradeDF.col("student_id"))
                        .drop(gradeDF.col("student_id"))

    //    2. The number of students passing and failing for each class.
      val perClassPassFailDF= joinDF.withColumn("pass", when(col("grade").notEqual("F"),1).otherwise(0))
                                  .withColumn("fail", when(col("grade").equalTo("F"),1).otherwise(0))
                                  .groupBy("class")
                                  .agg(
                                    sum("pass").as("total_pass_per_class"),
                                    sum("fail").as("total_fail_per_class")
                                  )

    writeDf(perClassPassFailDF,"assignment/pass-fails")
    log.info("students passing and failing for each class output done")

//    3. Class-wise topper names
    val window = Window.partitionBy("class").orderBy(col("total_marks").desc)

    val topperDF=joinDF.withColumn("topper", rank().over(window))
      .where("topper == 1")
      .drop("topper")

    writeDf(topperDF,"assignment/topper")
//    5. Names of the students failing in class 5-C
   val failDF= joinDF
      .filter(expr("class == '5-C'").and(expr("grade == 'F'")))

    writeDf(failDF,"assignment/fail-5C")

    log.info("program finished...!")

  }


  def writeDf(dataset: Dataset[Row], path:String): Unit ={
    dataset
      .coalesce(1)  // only for writing in single file
      .write
      .format("csv")
      .mode(SaveMode.Overwrite)
      .option("header","true")
      .option("path",path)
      .save()

  }

}
